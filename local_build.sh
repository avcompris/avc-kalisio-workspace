#!/bin/sh

# File: avc-kalisio-workspace/local_build.sh
#
# Use this script to build the Docker image locally.
#
# Note: The buildinfo file will not be filled, nor the image pushed.

set -e

date > buildinfo
echo "HOSTNAME: ${HOSTNAME}" >> buildinfo
echo "USER: ${USER}" >> buildinfo

docker build -t avcompris/kalisio-workspace .

echo "Done."
