_[Back to the home page](../README.md)_

# My first endpoint

In `skeleton/api`, add this to
`services.js`:

````
app.use(app.get('apiPath') + '/coco', (req, res, next) => {
    const response = {
        name: 'ratapoil'
    }
    res.json(response)
})
````

To be triggered only by `POST` methods,
implement a `create()` method in a service:

````
class CocoService {
   async create(data, params) {
      console.log('CocoService')
      return 'trololo'
   }
}

app.use(app.get('apiPath') + '/xcoco', new CocoService(), {
   methods: ['create'],
   events: ['coco-event']
})
````

