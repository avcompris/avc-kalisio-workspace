_[Back to the home page](../README.md)_

# Kakisio’s documentation

Here’s a table of contents of Kalisio’s
online documentation:

## kApp

* [kApp — Home](https://kalisio.github.io/kApp/)
  * [About](https://kalisio.github.io/kApp/about/introduction.html)
  * [Contributing](https://kalisio.github.io/kApp/about/contributing.html)
  * [License](https://kalisio.github.io/kApp/about/license.html)
  * [Contact](https://kalisio.github.io/kApp/about/contact.html)
* **Guides**
  * [Introduction](https://kalisio.github.io/kApp/guides/introduction.html)
  * [Tour](https://kalisio.github.io/kApp/guides/customizing/tour.html)
  * [PWA](https://kalisio.github.io/kApp/guides/customizing/pwa.html)
* **Reference**
  * [Configuring a kApp](https://kalisio.github.io/kApp/reference/configuration.html)


The live demo:
[https://kapp.dev.kalisio.xyz/](https://kapp.dev.kalisio.xyz/)

## KDK

* [KDK — Home](https://kalisio.github.io/kdk/)
  * [About](https://kalisio.github.io/kdk/about/introduction.html)
  * [Roadmap / Release notes](https://kalisio.github.io/kdk/about/roadmap.html) → Link to GitHub
  * [Contributing](https://kalisio.github.io/kdk/about/contributing.html)
  * [License](https://kalisio.github.io/kdk/about/license.html)
  * [Contact](https://kalisio.github.io/kdk/about/contact.html)
* **Guides**
  * [Introduction](https://kalisio.github.io/kdk/guides/introduction.html)

  
  * **The Basics**
      * [Introduction to KDK](https://kalisio.github.io/kdk/guides/basics/introduction.html)

  * **Development** 

      * [Setup your environment](https://kalisio.github.io/kdk/guides/development/setup.html) + Link to Skeleton
      * [Develop with KDK](https://kalisio.github.io/kdk/guides/development/develop.html) + Link to Skeleton
      * [Testing with KDK](https://kalisio.github.io/kdk/guides/development/test.html) + Link to Skeleton
      * [Configure your app](https://kalisio.github.io/kdk/guides/development/configure.html) → Link to Skeleton
      * [Deploy your app](https://kalisio.github.io/kdk/guides/development/deploy.html) → Link to Skeleton
      * [Publish your app](https://kalisio.github.io/kdk/guides/development/publish.html) + Link to Skeleton

* **[Architecture](https://kalisio.github.io/kdk/architecture/introduction.html)**
  * [Main concepts](https://kalisio.github.io/kdk/architecture/main-concepts.html)
  * [Global architecture](https://kalisio.github.io/kdk/architecture/global-architecture.html)
  * [Component view](https://kalisio.github.io/kdk/architecture/component-view.html)
  * [Data model-oriented view of the architecture](https://kalisio.github.io/kdk/architecture/data-model-view.html) 

* **[API](https://kalisio.github.io/kdk/api/introduction.html)**

   * **core**
   
      * [Overview](https://kalisio.github.io/kdk/api/core/introduction.html)
      * [Application](https://kalisio.github.io/kdk/api/core/application.html)
      * [Services](https://kalisio.github.io/kdk/api/core/services.html)
      * [Hooks](https://kalisio.github.io/kdk/api/core/hooks.html)
      * [Components](https://kalisio.github.io/kdk/api/core/components.html)
      * [Mixins](https://kalisio.github.io/kdk/api/core/mixins.html)
      * [Composables](https://kalisio.github.io/kdk/api/core/composables.html)

   * **map**

      * [Overview](https://kalisio.github.io/kdk/api/map/introduction.html)
      * [Services](https://kalisio.github.io/kdk/api/map/services.html)
      * [Hooks](https://kalisio.github.io/kdk/api/map/hooks.html)
      * [Components](https://kalisio.github.io/kdk/api/map/components.html)
      * [Mixins](https://kalisio.github.io/kdk/api/map/mixins.html)
      * [Map Mixins](https://kalisio.github.io/kdk/api/map/map-mixins.html)
      * [Globe Mixins](https://kalisio.github.io/kdk/api/map/globe-mixins.html)
      * [Composables](https://kalisio.github.io/kdk/api/map/composables.html)
      * 

* **[Tips](https://kalisio.github.io/kdk/tips/introduction.html)**

   * [Application development](https://kalisio.github.io/kdk/tips/app-development.html)
   * [Mobile configuration](https://kalisio.github.io/kdk/tips/mobile-configuration.html)
  

* **[Tools](https://kalisio.github.io/kdk/tools/introduction.html)**
  
   * [Command-line tools](https://kalisio.github.io/kdk/tools/cli.html)
   * [Browser-based tools](https://kalisio.github.io/kdk/tools/browsers.html)
   * [Documentation](https://kalisio.github.io/kdk/tools/documentation.html)
   * [Infrastructure](https://kalisio.github.io/kdk/tools/infrastructure.html)

   
## Skeleton

* [Skeleton — Home](https://kalisio.github.io/skeleton/)
  * [About](https://kalisio.github.io/skeleton/about/introduction.html)
  * [Contributing](https://kalisio.github.io/skeleton/about/contributing.html)
  * [License](https://kalisio.github.io/skeleton/about/license.html)
  * [Contact](https://kalisio.github.io/skeleton/about/contact.html)

* **Guides**
  * [Introduction](https://kalisio.github.io/skeleton/guides/introduction.html)
  * [Folder Structure](https://kalisio.github.io/skeleton/guides/structure.html)
  * [Installation](https://kalisio.github.io/skeleton/guides/installing.html)
  * [Setup your environment](https://kalisio.github.io/skeleton/guides/development/setup.html)
  * [Develop your app](https://kalisio.github.io/skeleton/guides/development/develop.html)
  * [Testing your app](https://kalisio.github.io/skeleton/guides/development/test.html)
  * [Configure your app](https://kalisio.github.io/skeleton/guides/development/configure.html)
  * [Deploy your app](https://kalisio.github.io/skeleton/guides/development/deploy.html)
  * [Publish your app](https://kalisio.github.io/skeleton/guides/development/publish.html)


## Articles

| Title | Date |
| :--- | :---: |
| [Kano geospatial visualizer v2 is out !](https://claustres.medium.com/kano-geospatial-visualizer-v2-is-out-4009f0fc94ea) | Jul 2, 2023 |
| [OAuth made easier with Feathers v4/v5, OpenID Connect and Keycloak](https://blog.feathersjs.com/oauth-made-easier-with-feathers-v4-v5-openid-connect-and-keycloak-1c0f575cbbec) | Sep 20, 2022 |
| [How To Fix Your Docker-Based Microservice Infrastructure](https://betterprogramming.pub/how-to-fix-your-docker-based-microservice-infrastructure-17ba34bb7561) | May 18, 2021 |
| [Learning Data Science From the Perspective of a Proficient Developer](https://betterprogramming.pub/learning-data-science-from-the-perspective-of-a-proficient-developer-59c258e4d7ae) | Dec 8, 2020 | 
| [Distributed Instant Logs Made Easy With Docker](https://betterprogramming.pub/distributed-instant-logs-made-easy-with-docker-b5e7f501f045) | Oct 28, 2020 |
| [What if it were easy to be smart?](https://byrslf.co/what-if-it-were-easy-to-be-smart-4e72ecb742e9) | Oct 19, 2020 |
| [What can COVID-19 tell us about our digital achievements and failures in France ?](https://claustres.medium.com/what-can-covid-19-tell-us-about-our-digital-achievements-and-failures-68f76bda1c17) | Apr 20, 2020 |
| [Why We Stopped Using So-Called Best Practices in Our CI/CD Process](https://betterprogramming.pub/why-we-stopped-using-so-called-best-practices-in-our-ci-cd-process-2ff09811f633) | Jan 21, 2020 |
| [Starting your own business as a therapy against bulls**t](https://claustres.medium.com/starting-your-own-business-as-a-therapy-against-bulls-t-14152bd0afcd) | Dec 14, 2019 | 
| [Mocking custom service queries with FeathersJS](https://blog.feathersjs.com/mocking-custom-service-queries-with-feathersjs-3aae74003259) | Dec 10, 2019 |
| [Développeurs libres, soyez fiers d’être les artisans du numérique !](https://claustres.medium.com/d%C3%A9veloppeurs-libres-soyez-fiers-d%C3%AAtre-les-artisans-du-num%C3%A9rique-4474fc5d4781) (French) | Dec 8, 2019 |
| [A use case of microservices with FeathersJS: building a geospatial platform](https://blog.feathersjs.com/a-use-case-of-microservices-with-feathersjs-building-a-geospatial-platform-56373604db71) | Jun 10, 2019 |
| [Why software development should help you in life… and politics?](https://claustres.medium.com/why-software-development-should-help-you-in-life-and-politics-9c298801aec7) | Jan 10, 2019 |
| [How to build a map print service in minutes](https://claustres.medium.com/how-to-build-a-map-print-service-in-minutes-c5a24b1f0f41) | Jun 27, 2018 |
| [Enterprise-grade authentication using AWS Cognito and OneLogin with FeathersJS v3](https://blog.feathersjs.com/enterprise-grade-authentication-using-aws-cognito-and-onelogin-with-feathersjs-d4c6f46ab123) | Jun 17, 2018 |
| [FeathersJS in production: password policy and rate limiting](https://blog.feathersjs.com/feathersjs-in-production-password-policy-and-rate-limiting-32c9874dc563) | May 25, 2018 |
| [The Evolution of Computer Science: from the Static to the Dynamic Paradigm](https://claustres.medium.com/the-evolution-of-computer-science-from-the-static-to-the-dynamic-paradigm-139c0dcd3245) | Apr 20, 2018 |
| [Stress testing your FeathersJS application like in production](https://blog.feathersjs.com/stress-testing-your-feathersjs-application-like-in-production-4b8611ee8d9e) | Mar 15, 2018
| [Please Stop Using console.log() for Debugging — It’s Broken](https://betterprogramming.pub/please-stop-using-console-log-its-broken-b5d7d396cf15) | Feb 27, 2018 |
| [A Common Misconception About Async/Await in JavaScript](https://betterprogramming.pub/a-common-misconception-about-async-await-in-javascript-33de224bd5f) | Feb 19, 2018 |
| [Experience Is Dead, Welcome to Unlearning](https://betterprogramming.pub/experience-is-dead-welcome-unlearning-fda05076dfb5) | Feb 12, 2018 |
| [Why You Should Not Worry About the Rise Of AI](https://claustres.medium.com/the-developer-philosopher-why-you-should-not-worry-about-the-rise-of-ai-in-2018-1494e0c40e1a) | Dec 31, 2017 |
| [The Next Step Towards Conscious AI Should Be Awareness](https://towardsdatascience.com/the-next-step-towards-conscious-ai-should-be-awareness-91fbb1cb005f) | Dec 16, 2017 |
| [A minimalist ETL using FeathersJS — Part 2](https://blog.feathersjs.com/a-minimalist-etl-using-feathersjs-part-2-6aa89bd73d66) | Dec 4, 2017 |
| [A minimalist ETL using FeathersJS — Part 1](https://blog.feathersjs.com/a-minimalist-etl-using-feathersjs-part-1-1d56972d6500) | Nov 29, 2017 |
| [How To Decide If MongoDB Is Right For You](https://betterprogramming.pub/mongodb-insights-20e36c8f2fcd) | Aug 29, 2017 |
| [Access control strategies with FeathersJS](https://blog.feathersjs.com/access-control-strategies-with-feathersjs-72452268739d) | Aug 5, 2017 |
| [How to setup OAuth flow with FeathersJS v3](https://blog.feathersjs.com/how-to-setup-oauth-flow-with-featherjs-522bdecb10a8) | Jul 22, 2017 |
| [FeathersJS in production: configuration, API prefixing, logging and error catching](https://blog.feathersjs.com/feathersjs-in-production-configuration-api-prefixing-logging-and-error-catching-2a80e044e233) | Jun 30, 2017 |