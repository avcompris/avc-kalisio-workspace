_[Back to the home page](../README.md)_

# Skeleton Deployment

In a Docker container running our image:

````
$ whoami
develop
````

## 1. Get the code

We start with the KDK:

````
$ cd
$ git clone https://github.com/kalisio/kdk.git
$ cd kdk
$ yarn install
$ yarn link
````

Then the Skeleton’s API:

````
$ cd
$ git clone https://github.com/kalisio/skeleton.git
$ cd skeleton/api
$ yarn install
$ yarn link @kalisio/kdk
````

Then the Skeleton’s frontend:

````
$ cd
$ cd skeleton
$ yarn install
$ yarn link @kalisio/kdk
````

## 2. Run

First you need MongoDB:

````
$ docker run -d -p 27017:27017 mongo:4.2
````

To start the API:

````
$ cd
$ cd skeleton/api
$ export DB_URL=mongodb://172.17.0.1:27017/skeleton
$ yarn dev
````

This starts the server.

Check:

````
$ curl --location 'http://localhost:8081/api/authentication' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "strategy": "local",
        "email": "kalisio@kalisio.xyz",
        "password": "Pass;word1"
    }'
````

Tbis will give you an access token, e.g.:

````
...
"accessToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6ImFjY2VzcyJ9.eyJpYXQiOjE3MTIwNzY2ODYs
ImV4cCI6MTcxMjE2MzA4NiwiYXVkIjoia2FsaXNpbyIsImlzcyI6ImthbGlzaW8iLCJzdWIiOiI2NjBj
MzZmOTUyODIzMjAwZjYxOTg5YTgiLCJqdGkiOiIwZGZjMmUxMi04ZjVhLTQ3NjEtOGMzOS1mYWU3ZTlh
Yzg1ZWYifQ.83ITc0mE3MDJOTIsW3F9c2n9IfY1M5sQX4ujgdqxHXA"
...
````


You can then use this token in such calls:

````
$ curl --location 'http://localhost:8081/api/users' \
    --header 'Authorization: Bearer <token>'
````

See also:
API / Introduction / [Testing](https://kalisio.github.io/kdk/api/introduction.html#testing)

Note: In our example, the access token’s payload decodes as follow
(thanks to [jwt.io](https://jwt.io)’s tools):

````
{
  "iat": 1712076686,
  "exp": 1712163086,
  "aud": "kalisio",
  "iss": "kalisio",
  "sub": "660c36f952823200f61989a8",
  "jti": "0dfc2e12-8f5a-4761-8c39-fae7e9ac85ef"
}
````

Its header decodes as follows:

````
{
  "alg": "HS256",
  "typ": "access"
}
````

To start the frontend:

````
$ cd
$ cd skeleton
$ export PORT=8081
$ yarn dev
````

This starts the server.

Check:

````
$ curl http://localhost:8080
````

Also, the same API calls can be passed through the frontend:

````
$ curl --location 'http://localhost:8080/api/users' \
    --header 'Authorization: Bearer <token>'
````


## 3. Mix all the elements

Actually, no need of SSH tunnels: The frontend on `8080` does
call the API on `8081` itself. The user only sees `8080`.

## 4. How to manage users through the API

Given `H="Authentication: Bearer <token>`, we go like this:

To retrieve the user list:

````
$ curl --header "${H}" http://localhost:8081/api/users
````

The server’s response (200):

````
{
    "total": 1,
    "limit": 10,
    "skip": 0,
    "data":[{
        "_id": "660c36f952823200f61989a8",
        "email": "kalisio@kalisio.xyz",
        "profile": {
            "name": "Kalisio",
            "description": "kalisio@kalisio.xyz"
        }
    }]
}
````

To create a user:

````
$ curl --header "${H}" -X POST --location http://localhost:8081/api/users \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "email": "toto@toto.com",
        "password": "Pass;word1",
        "name": "Toto"
    }'
````

The server’s response (201):

````
{
    "_id": "660c435152823200f61989aa",
    "email": "toto@toto.com",
    "profile": {
        "name": "Toto",
        "description": "toto@toto.com"
    }
}
````

To delete a user:

````
$ curl --header "${H}" -X DELETE \
    http://localhost:8081/api/users/660c435152823200f61989aa
````

The server’s response (403):

````
{
    "name": "Forbidden",
    "message": "You are not allowed to perform remove operation on users",
    "code": 403,
    "className": "forbidden"
}
````

Ouch. I thought I used the token created for the admin superuser.

## 5. Some other endpoints

* `http://localhost:8081/api/capabilities`

````
{
    "name": "teams",
    "domain": "http://localhost:8080",
    "version": "1.0.0"
}
````



