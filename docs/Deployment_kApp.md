_[Back to the home page](../README.md)_

# kApp Deployment

In a Docker container running our image:

````
$ whoami
develop
````

## 1. Get the code

We start with the KDK:

````
$ cd
$ git clone https://github.com/kalisio/kdk.git
$ cd kdk
$ yarn install
````

Then the kApp’s API:

````
$ cd
$ git clone https://github.com/kalisio/kApp.git
$ cd kApp/api
$ yarn install
$ yarn link @kalisio/kdk
````

Then the kApp’s frontend:

````
$ cd
$ cd kApp
$ yarn install
$ yarn link @kalisio/kdk
````

## 2. Run

First you need MongoDB:

````
$ docker run -d -p 27017:27017 mongo:4.2
````

To start the API:

````
$ cd
$ cd kApp/api
$ export DB_URL=mongodb://172.17.0.1:27017/kapp
$ yarn dev
````

This starts the server.

Check:

````
$ curl http://localhost:8081/api/authentication
````

Should return 405 with a JSON response
containing “Method `find` is not supported by this endpoint.”



See also:
API / Introduction / [Testing](https://kalisio.github.io/kdk/api/introduction.html#testing)

To start the frontend:

````
$ cd
$ cd kApp
$ # export PORT=8081
$ yarn dev
````

This starts the server.

Check:

````
$ curl http://localhost:8080
````

## 3. Mix all the elements

Actually, no need of SSH tunnels: The frontend on `8080` does
call the API on `8081` itself. The user sees only `8080`.

