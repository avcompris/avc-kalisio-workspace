# File: avc-kalisio-workspace/Dockerfile
#
# Use to build the image: avcompris/kalisio-workspace

FROM debian:12
MAINTAINER david.andriana@avantage-compris.com


# 1. STANDARD PACKAGES

RUN apt-get update --fix-missing

RUN apt-get install -y \
	apt-utils \
	curl \
	git \
	gnupg2 \
	iputils-ping \
	jq \
	libxml2-utils \
	software-properties-common \
	sudo \
	vim


# 2. DOCKER

RUN curl -sSL https://get.docker.com/ | sh


# 3. DOCKER-COMPOSE

RUN curl \
        -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" \
        -o /usr/local/bin/docker-compose

RUN chmod +x /usr/local/bin/docker-compose

RUN docker-compose --version


# 4. JAVA, MAVEN

RUN apt-get install -y \
    openjdk-17-jdk \
    maven


# 5. LOCALES

RUN apt-get install -y locales

RUN locale-gen "en_US.UTF-8"

RUN echo "LANG=en_US.UTF-8" >> /etc/default/locale
RUN echo "LC_ALL=en_US.UTF-8" >> /etc/default/locale


# 6. USERS

RUN useradd -m -s /bin/bash develop

RUN echo "develop ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/10-develop

RUN usermod -aG docker develop


# 7. NVM, NODE, NPM, YARN

USER develop
WORKDIR /home/develop

# RUN apt-get install -y nodejs # This installs Node 10.24.0, but we need >= 16.0.0
# RUN apt-get install -y yarnpkg # This installs an old yarn for NodeJS
# RUN apt-get install -y yarn # This installs cmdtest

RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash

ENV NVM_DIR=/home/develop/.nvm

RUN . "${NVM_DIR}/nvm.sh" && nvm install 20

RUN . "${NVM_DIR}/nvm.sh" && node -v # v20.12.2

RUN . "${NVM_DIR}/nvm.sh" && npm -v # 10.5.0

RUN . "${NVM_DIR}/nvm.sh" && npm install --global yarn

RUN . "${NVM_DIR}/nvm.sh" && yarn -v # 1.22.22

RUN if [ `ls -1 ${NVM_DIR}/versions/node` != v20.12.2 ]; then \
       echo "NODE_VERSION: Expected: v20.12.2, but was: `ls -1 ${NVM_DIR}/versions/node`" && exit 1; \
    fi

ENV NODE_VERSION=v20.12.2

ENV PATH=${PATH}:${NVM_DIR}/versions/node/${NODE_VERSION}/bin


# 8. BUILDINFO

USER root

COPY buildinfo /


# 9. END

USER develop
WORKDIR /home/develop
