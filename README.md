# avc-kalisio-workspace


Builds a Docker image
for an exploration of the Kalisio KDK and kApp from a developer perspective.

The image is « avcompris/kalisio-workspace ». It contains:

* Debian 12.5
* Some standard utilities: vim, git, curl, jq
* Docker
* Node v20.12.0 — npm 10.5.0 — yarn 1.22.22
* OpenJDK 17.0.10 — Maven 3.8.7
* A `develop` user, sudoer without password

## How to build the image

You need Docker.

### Locally

Use [`local_build.sh`](./local_build.sh)

### CI

See [`.gitlab-ci.yml`](./.gitlab-ci.yml)

When run by Jenkis or GitLab-CI, a `buildinfo`
file is added to the image with such info as:

* `GIT_COMMIT` / `CI_COMMIT_SHA`
* `JOB_NAME` / `CI_PROJECT_NAME`
* `BUILD_NUMBER` / `CI_JOB_ID`
* `BUILD_TIMESTAMP`
* `NODE_NAME` / `CI_RUNNER_ID`

This allows to retrieve those info by inspecting
the image, or with the following command:

````
$ docker run avcompris/kalisio-workspace cat /buildinfo
````

Example of output:

````
CI_COMMIT_SHA: 5fd3daef4b55d671cecef172e6ce695abc1b1731
CI_PROJECT_NAME: avc-kalisio-workspace
CI_JOB_ID: 6516682088
BUILD_TIMESTAMP: Sun Mar 31 10:05:51 UTC 2024
CI_RUNNER_ID: 23203268
````

## How to use

Say you have Docker running on your workstation
on the `172.17.0.1` local IP Address, you could
do this to use this image:

````
$ docker run -ti -e DOCKER_HOST=172.17.0.1 \
     avcompris/kalisio-workspace bash
````

And in order to import your local SSH key:

````
$ docker run -ti -e DOCKER_HOST=172.17.0.1 \
     -v ${HOME}/.ssh/:/home/develop/.ssh:ro \
     avcompris/kalisio-workspace bash
````



## What to do then

### Skeleton

Go to [Kalisio / Skeleton / Guides](https://kalisio.github.io/skeleton/guides/introduction.html)
and follow their instructions.

Here’s our own attempt: [Skeleton Deployment](docs/Deployment_Skeleton.md)

See also: [My first endpoint](docs/First_Endpoint.md)

### kApp

Go to [Kalisio / kApp / Guides](https://kalisio.github.io/kApp/guides/introduction.html)
and follow their instructions.

Here’s our own attempt: [kApp Deployment](docs/Deployment_kApp.md)


## Kakisio’s documentation

Here’s a table of contents of Kalisio’s
online documentation:
[Kakisio’s documentation](docs/Kalisio_docs_toc.md)
